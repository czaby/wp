# redundant WP cluster

- will setup first docker on all nodes  
- deploy the containers with docker-compose to form a redundant WP cluster with a loadbalancer
- then test the site

infrastructure is copied from my raspberry pi cluster setup  
DB setup is from https://galeracluster.com/library/documentation/docker.html

## Setup with 
> ansible-playbook site.yml

## remove site with
> ansible-playbook cleanup.yml

